const Sport = require('../models/sport.model')
const resp = require('../middlewares/responseHandling')


exports.sportCreate = (req, res) => {
    Sport.create({
        sportName: req.body.sportName
    })
    .then(createSport => {
        resp(res, true, 'sport created', createSport)
    })
    .catch(err => {
        resp(res, false, 'fail create sport', err)
    })
}

exports.sportShowAll = (req, res) => {
    Sport.find({})
        .then(allSport => {
            resp(res, true, 'all sport showed', allSport)
        })
        .catch(err => {
            resp(res, false, 'cannot show all sport', err)
        })
}

exports.sportShow = (req, res) => {
    Sport.findById(req.params.id)
        .then(detailSport => {
            if (detailSport) {
                resp(res, true, 'detail sport showed', detailSport)
            } else {
                resp(res, false, 'detail sport not avaliable')
            }
        })
        .catch(err => {
            resp(res, false, 'detail sport can not be showed', err)
        })
}

exports.sportEdit = (req, res) => {
    Sport.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true, useFindAndModify: false, runValidators: true })
    .then(updateSport => {
        resp(res, true, 'sport detail succes to update', updateSport)
    })
    .catch(err => {
        resp(res, false, 'sport detail failed to update', err)
    })
}

exports.sportDelete = (req, res) => {
    Sport.findByIdAndRemove(req.params.id, { useFindAndModify: false })
        .then(delSport => {
            if (delSport) {
                resp(res, true, 'sport deleted', delSport)
            } else {
                resp(res, false, 'sport yg terhapus tak bisa ditampilkan', err)
            }
        })
        .catch(err => {
            resp(res, false, 'failed delete sport', err)
        })
}
