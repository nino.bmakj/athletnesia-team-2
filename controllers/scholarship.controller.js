const Scholarship = require('../models/scholarship.model');
const Club = require('../models/club.model');
const Athlete = require('../models/athlete.model');
const User = require('../models/user.model');
const Sport = require('../models/sport.model');
const responseHandling = require('../middlewares/responseHandling');


exports.scholarshipCreate = (req, res) => {
  const clubId = req.clubId

  const newScholarship = new Scholarship({
    scholarshipName: req.body.scholarshipName,
    clubId: clubId,
    quota: req.body.quota,
    description: req.body.description,
    prerequisites: req.body.prerequisites,
    startDate: req.body.startDate,
    endDate: req.body.endDate,
  });

  newScholarship.save()
    .then(savedData => {
      return Club.findByIdAndUpdate(savedData.clubId, {$push: {scholarshipOffered: savedData._id}}, {new: true});
    })
    .then(updatedClub => responseHandling(res, true, "scholarship is saved", updatedClub))
    .catch(err => responseHandling(res, false, "something went wrong, no data is retrieved", err));
}

exports.scholarshipShowAll = (req, res) => {
  Scholarship.find()
    .populate({
      path: 'clubId',
      select: ['clubLogo', 'clubAddress'],
      populate: [
        {path: 'userId', model: 'User', select: 'firstName'},
        {path: 'sportId', model: 'Sport', select: 'sportName'}
      ]
    })
    .then(data => {
      if (data.length === 0) responseHandling(res, true, "there is no scholarship available, yet", data);
      else responseHandling(res, true, "scholarships data are retrieved", data);
    })
    .catch(err => responseHandling(res, false, "something went wrong, no data is retrieved", err));
}

exports.scholarshipDetail = (req, res) => {
  Scholarship.findById(req.params.id)
    .populate({
      path: 'clubId',
      select: ['clubLogo', 'clubAddress', 'clubPhone', 'userId', 'sportId'],
      populate: [
        {path: 'userId', model: 'User', select: 'firstName'},
        {path: 'sportId', model: 'Sport', select: 'sportName'}
      ]
    })
    .populate({
      path: 'appliedAthlete',
      select: 'profilePicture',
      populate: {path: 'userId', model: 'User', select: ['firstName', 'lastName']}
    })
    .then(data => {
      if (data !== null) responseHandling(res, true, "scholarship data are retrieved", data);
      else responseHandling(res, false, "scholarships data are not available");
    })
    .catch(err => responseHandling(res, false, "something went wrong, no data is retrieved", err));
}

exports.scholarshipUpdate = (req, res) => {
  Scholarship.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true})
    .then(updated => responseHandling(res, true, "scholarship data is updated", updated))
    .catch(err => responseHandling(res, false, "something went wrong, no data is retrieved", err));
}

exports.scholarshipDelete = (req, res) => {
  Scholarship.findByIdAndDelete(req.params.id, (err, deletedScholar) => {
    if (err) responseHandling(res, false, "something went wrong, no data is retrieved", err)
    else {
      if (deletedScholar !== null) deleteFromAthlete(deletedScholar);
      else responseHandling(res, false, "there is no associated data available");
    }
  });

  function deleteFromAthlete(deletedScholar) {
    Athlete.updateMany(
      {appliedScholarship: deletedScholar._id}, 
      {$pull: {appliedClub: deletedScholar.clubId, appliedScholarship: deletedScholar._id}},
      {$new: true},
      (err, result) => {
        if (err) responseHandling(res, false, "something went wrong, no data is retrieved");
        else if (result['ok'] === 1) deleteFromClub(deletedScholar);
        else responseHandling(res, false, "there is no associated data available");
      })  
  }

  function deleteFromClub(deletedScholar) {
    Club.findByIdAndUpdate(
      deletedScholar.clubId,
      {$pull: {scholarshipOffered: deletedScholar._id, appliedAthlete: deletedScholar.appliedAthlete}},
      {$new: true},
      (err, result) => {
        if (err) responseHandling(res, false, "something went wrong, no data is retrieved", err);
        else {
          if (result !== null) handleFunc(deletedScholar, result);
          else responseHandling(res, false, "there is no associated data available"); 
        }
      });
  }

  function handleFunc(deletedScholar, result) {
    responseHandling(res, true, "scholarship has been deleted", {deletedScholar, result});
  }
}

exports.scholarshipApply = (req, res) => {
  // this will take care of route '/scholarships/:id/apply'
  Scholarship.findById(req.params.id, (err, data) => {
    if (err) responseHandling(res, false, "something went wrong, no data is retrieved", err);
    else {
      if (data !== null) updateScholarship(data);
      else responseHandling(res, false, "there is no scholarship available");
    }
  });

  function updateScholarship(scholarshipData) {
    scholarshipData.updateOne({
      appliedAthlete: [...scholarshipData.appliedAthlete, req.athleteId]
    }, (err, data) => {
      if (err) responseHandling(res, false, "something went wrong, no data is retrieved", err);
      else {
        updateClub(scholarshipData);
      }
    });
  }

  function updateClub(updatedScholarship) {
    Club.findByIdAndUpdate(updatedScholarship.clubId, {$push: {appliedAthlete: req.athleteId}}, {new: true}, (err, data) => {
      if (err) responseHandling(res, false, "something went wrong, no data is retrieved", err);
      else if (data !== null) updateAthlete(updatedScholarship);
      else responseHandling(res, false, "there is no associated club");
    })
  }

  function updateAthlete(updatedScholarship) {
    Athlete.findOneAndUpdate(
      {userId: req.userId}, 
      {$push: {appliedScholarship: updatedScholarship._id, appliedClub: updatedScholarship.clubId}}, 
      {new: true}, handleFunc);
  }

  function handleFunc(err, updatedAthlete) {
    if (err) responseHandling(res, false, "something went wrong, no data is retrieved", err);
    else {
      if (updatedAthlete !== null) responseHandling(res, true, "data is updated", updatedAthlete);
      else responseHandling(res, false, "there is no athlete available");
    }
  }
}

exports.scholarshipRemoveApply = (req, res) => {
  Scholarship.findByIdAndUpdate(req.params.id, {$pull: {appliedAthlete: {$in: req.athleteId}}}, {new: true}, 
    (err, updatedScholarship) => {
      if (err) responseHandling(res, false, "something went wrong", err);
      else if (updatedScholarship !== null) updateAthlete(updatedScholarship);
      else responseHandling(res, false, "scholarship data is not available", null);
    });

  function updateAthlete(updatedScholarship) {
    Athlete.findByIdAndUpdate(req.athleteId, {$pull: {appliedScholarship: {$in: updatedScholarship._id}}}, 
      {new: true}, 
      (err, updatedAthlete) => {
        if (err) responseHandling(res, false, "somethign went wrong", err);
        else if (updatedAthlete !== null) {
          responseHandling(res, true, "remove process has succeeded", [updatedScholarship, updatedAthlete]);
        }
        else responseHandling(res, false, "athlete data is not found", null);
      });
  }
}