const Skill = require('../models/skill.model')
const resp = require('../middlewares/responseHandling')


exports.skillCreate = (req, res) => {
    Skill.create({
        skill: req.body.skill
    })
    .then(createSkill => {
        resp(res, true, 'skill created', createSkill)
    })
    .catch(err => {
        resp(res, false, 'fail create skill', err)
    })
}

exports.skillShowAll = (req, res) => {
    Skill.find({})
        .then(AllSkill => {
            resp(res, true, 'all skill showed', AllSkill)
        })
        .catch(err => {
            resp(res, false, 'cannot show all skill', err)
        })
}

exports.skillShow = (req, res) => {
    Skill.findById(req.params.id)
        .then(detailSkill => {
            if (detailSkill) {
                resp(res, true, 'detail skill showed', detailSkill)
            } else {
                resp(res, false, 'detail skill not avaliable')
            }
        })
        .catch(err => {
            resp(res, false, 'detail skill can not be showed', err)
        })
}

exports.skillEdit = (req, res) => {
    Skill.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true, useFindAndModify: false, runValidators: true })
    .then(updateSkill => {
        resp(res, true, 'skill detail succes to update', updateSkill)
    })
    .catch(err => {
        resp(res, false, 'skill detail failed to update', err)
    })
}

exports.skillDelete = (req, res) => {
    Skill.findByIdAndRemove(req.params.id, { useFindAndModify: false })
        .then(delSkill => {
            if (delSkill) {
                resp(res, true, 'skill deleted', delSkill)
            } else {
                resp(res, false, 'skill yg terhapus tak bisa ditampilkan', err)
            }
        })
        .catch(err => {
            resp(res, false, 'failed delete skill', err)
        })
}
