const Club = require('../models/club.model')
const User = require('../models/user.model')
const Athlete = require('../models/athlete.model')
const Scholarship = require('../models/scholarship.model');
const bcrypt = require('bcrypt')
const resp = require('../middlewares/responseHandling')

exports.clubCreate = (req, res) => {
    User.findOne({ email: req.body.email })
        .then(user => {
            if (user) resp(res, false, "the email has already been used");
            else hashPassword();
        })
        .catch(err => resp(res, false, "something went wrong, no data is retrieved", err));

    function hashPassword() {
        const password = req.body.password;
        const saltrounds = 10;

        bcrypt.hash(password, saltrounds, createUser)
    }

    function createUser(err, hashedPassword) {
        if (err) resp(res, false, "something went wrong, no data is retrieved", err);
        else {
            User.create({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email,
                password: hashedPassword
            }, createClub);
        }
    }

    function createClub(err, data) {
        if (err) resp(res, false, "something went wrong, no data is retrieved", err);
        else {
            Club.create({
                userId: data._id
            }, handleFunc);
        }
    }

    function handleFunc(err, result) {
        if (err) resp(res, false, "something went wrong, no data is retrieved", err);
        else resp(res, true, "user is created", result);
    }
}

exports.clubShowAll = (req, res) => {
    Club.find({})
        .populate({ path: 'userId', select: 'firstName' })
        .populate({ path: 'sportId', select: 'sportName' })
        .populate({ path: 'scholarshipOffered', select: ['scholarshipName', 'appliedAthlete'] })
        .then(allClub => {
            if (allClub.length > 0) resp(res, true, 'all club showed', allClub);
            else if (allClub.length === 0) resp(res, true, 'there is no club available, yet', allClub);
        })
        .catch(err => {
            resp(res, false, 'cannot show all club', err)
        })
}

exports.clubShow = (req, res) => {
    Club.findById(req.params.id)
        .populate({ path: 'userId', select: ['firstName', 'lastName', 'email'] })
        .populate({ path: 'sportId', select: 'sportName' })
        .populate({ path: 'scholarshipOffered', select: ['scholarshipName', 'startDate', 'endDate', 'appliedAthlete'] })
        .populate({
            path: 'appliedAthlete',
            select: 'profilePicture',
            model: 'Athlete',
            populate: {path: 'userId', model: 'User', select: ['firstName', 'lastName']}
        })
        .populate({
            path: 'acceptedAthlete',
            select: 'profilePicture',
            model: 'Athlete',
            populate: {path: 'userId', model: 'User', select: ['firstName', 'lastName']}
        })
        .then(detailClub => {
            if (detailClub) {
                resp(res, true, 'detail club is shown', detailClub)
            } else {
                resp(res, false, 'detail club is not available')
            }
        })
        .catch(err => {
            resp(res, false, 'detail club can not be shown', err)
        })
}

exports.clubEdit = (req, res) => {
    Club.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true, useFindAndModify: false, runValidators: true })
        .then(updateClub => {
            resp(res, true, 'club detail success to update', updateClub)
        })
        .catch(err => {
            resp(res, false, 'club detail failed to update', err)
        })
}

exports.clubDelete = (req, res) => {
    const id = req.params.id;

    Club.findByIdAndDelete(id, (err, deletedClub) => {
        if (err) resp(res, false, "something went wrong", err);
        else {
            if (deletedClub.scholarshipOffered.length > 0) deleteScholarships(deletedClub);
            else deleteFromUser(deletedClub);
        }
    });

    function deleteScholarships(deletedClub) {
        Scholarship.deleteMany({ clubId: deletedClub._id }, (err, deleted) => {
            if (err) resp(res, false, "something went wrong, no data is retrieved", err);
            else deleteFromAthlete(deletedClub);
        })
    }

    function deleteFromAthlete(deletedClub) {
        const promises = Promise.all([
            Athlete.updateMany(
                { appliedClub: deletedClub._id },
                { $pull: { appliedClub: deletedClub._id } },
                { new: true }
            ).exec(),
            Athlete.updateMany(
                { acceptedClub: deletedClub._id },
                { $pull: { acceptedClub: deletedClub._id } },
                { new: true }
            )
            .exec()
        ]);

        promises
            .then(promisesArr => {
                const appliedClub = promisesArr[0];
                const acceptedClub = promisesArr[1];

                let result1;
                appliedClub
                    .then(deleted => result1 = deleted)
                    .catch(err => resp(res, false, "something went wrong", err));

                let result2;
                acceptedClub
                    .then(deleted => result2 = deleted)
                    .catch(err => resp(res, false, "something went wrong", err));

                const returnObj = {
                    results: [result1, result2],
                    deletedClub: deletedClub
                }

                return returnObj;
            })
            .then(obj => deleteFromUser(obj.deletedClub))
            .catch(err => resp(res, false, "something went wrong", err));
    }

    function deleteFromUser(deletedClub) {
        User.findByIdAndDelete(deletedClub.userId, (err, deletedUser) => {
            if (err) resp(res, false, "something went wrong, no data is retrieved", err);
            else resp(res, true, "club is deleted", { deletedClub, deletedUser });
        });
    }
}

exports.clubApply = (req, res) => {
    // this controller will take care of route '/club/:id/apply' 
    Club.findByIdAndUpdate(req.params.id, {$push: {appliedAthlete: req.athleteId}}, {new: true}, (err, updatedClub) => {
        if (err) resp(res, false, "something went wrong", err);
        else if (updatedClub !== null) updateAthlete(updatedClub);
        else resp(res, false, "club is not found", null);
    });

    function updateAthlete(updatedClub) {
        Athlete.findByIdAndUpdate(athleteId, {$push: {appliedClub: updatedClub._id}}, {new: true}, (err, updatedAthlete) => {
            if (err) resp(res, false, "something went wrong", err);
            else if (updatedAthlete !== null) resp(res, true, "applying process has succeed", [updatedClub, updatedAthlete]);
            else resp(res, false, "athlete is not found", null);
        });
    }
}

exports.clubRemoveApply = (req, res) => {
    // this controller will take care of route '/club/:id/remove'
    Club.findByIdAndUpdate(req.params.id, {$pull: {appliedAthlete: {$in: req.athleteId}}}, {new: true}, (err, updatedClub) => {
        if (err) resp(res, false, "something went wrong", err);
        else if (updatedClub !== null) updateAthlete(updatedClub);
        else resp(res, false, "club is not found", err);
    });

    function updateAthlete(updatedClub) {
        Athlete.findByIdAndUpdate(req.athleteId, {$pull: {appliedClub: {$in: updatedClub._id}}}, {new: true}, (err, updatedAthlete) => {
            if (err) resp(res, false, "something went wrong", err);
            else if (updatedAthlete !== null) resp(res, true, "removing process has succeed", [updatedClub, updatedAthlete]);
            else resp(res, false, "athlete is not found", null);
        });
    }
}

exports.login = (req, res) => {
    const data = {
        token: req.data.token,
        clubId: req.clubId,
        userId: req.userId
    }

    resp(res, true, "you are logging in", data);
}