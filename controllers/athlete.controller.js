const Athlete = require('../models/athlete.model')
const User = require('../models/user.model')
const Scholarship = require('../models/scholarship.model')
const Club = require('../models/club.model')
const bcrypt = require('bcrypt')
const resp = require('../middlewares/responseHandling')

exports.athleteCreate = (req, res) => {
    User.findOne({ email: req.body.email })
        .then(user => {
            if (user) resp(res, false, "the email has already been used");
            else hashPassword();
        })
        .catch(err => resp(res, false, "something went wrong, no data is retrieved", err));

    function hashPassword() {
        const password = req.body.password;
        const saltrounds = 10;

        bcrypt.hash(password, saltrounds, createUser)
    }

    function createUser(err, hashedPassword) {
        if (err) resp(res, false, "something went wrong, no data is retrieved", err);
        else {
            User.create({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email,
                password: hashedPassword,
                userType: req.body.userType
            }, createAthlete);
        }
    }

    function createAthlete(err, data) {
        if (err) resp(res, false, "something went wrong, no data is retrieved", err);
        else {
            Athlete.create({
                userId: data._id
            }, handleFunc);
        }
    }

    function handleFunc(err, result) {
        if (err) resp(res, false, "something went wrong, no data is retrieved", err);
        else resp(res, true, "user is created", result);
    }
}

exports.athleteShowAll = (req, res) => {
    Athlete.find({})
        .populate({ path: 'userId', select: ['firstName', 'lastName'] })
        .populate({ path: 'sportId', select: 'sportName' })
        .populate({ path: 'skillId', select: 'skill' })
        .then(allAth => {
            resp(res, true, 'all athletes are shown', allAth)
        })
        .catch(err => {
            resp(res, false, 'cannot show all athletes', err)
        })
}

exports.athleteShow = (req, res) => {
    Athlete.findById(req.params.id)
        .populate({path: 'userId', select: ['firstName', 'lastName', 'email']})
        .populate({path: 'sportId', select: 'sportName'})
        .populate({path: 'skillId', select: 'skill'})
        .populate({
            path: 'appliedClub',
            select: ['clubLogo', 'clubAddress'],
            populate: [
                {path: 'userId', model: 'User', select: 'firstName'},
                {path: 'sportId', model: 'Sport', select: 'sportName'}
            ]
        })
        .populate({
            path: 'acceptedClub',
            select: ['clubLogo', 'clubAddress'],
            populate: [
                {path: 'userId', model: 'User', select: 'firstName'},
                {path: 'sportId', model: 'Sport', select: 'sport'}
            ]
        })
        .populate({
            path: 'appliedScholarship',
            select: 'scholarshipName',
            populate: {
                path: 'clubId',
                model: 'Club',
                select: 'clubLogo',
                populate: {path: 'userId', model: 'User', select: 'firstName'}
            }
        })
        .then(detailAth => {
            if (detailAth) {
                resp(res, true, 'detail athlete is shown', detailAth)
            } else {
                resp(res, false, 'detail athlete is not avaliable')
            }
        })
        .catch(err => {
            resp(res, false, 'detail athlete can not be shown', err)
        })
}

exports.athleteEdit = (req, res) => {
    Athlete.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true, useFindAndModify: false, runValidators: true })
        .then(updateAth => {
            resp(res, true, 'athlete detail has been successfully updated', updateAth)
        })
        .catch(err => {
            resp(res, false, 'athlete detail update has been failed', err)
        })
}

exports.athleteDelete = (req, res) => {
    const id = req.params.id;

    Athlete.findByIdAndDelete(id, (err, deletedAthlete) => {
        if (err) resp(res, false, "something went wrong", err);
        else {
            if (deletedAthlete.appliedScholarship.length > 0) deleteFromScholarship(deletedAthlete);
            else deleteFromUser(deletedAthlete);
        }
    });

    function deleteFromScholarship(deletedAthlete) {
        Scholarship.updateMany(
            { appliedAthlete: {$in: deletedAthlete._id } }, 
            { $pull: { appliedAthlete: {$in: deletedAthlete._id} } }, 
            { new: true }, 
            (err, deleted) => {
                if (err) resp(res, false, "something went wrong", err);
                else deleteFromClub(deletedAthlete);
            }
        );
    }

    function deleteFromClub(deletedAthlete) {
        const promises = Promise.all([
            Club.updateMany(
                { appliedAthlete: { $in: deletedAthlete._id } },
                { $pull: { appliedAthlete: { $in: deletedAthlete._id } } }, 
                { new: true }
            ).exec(),
            Club.updateMany(
                { acceptedAthlete: { $in: deletedAthlete._id } }, 
                { $pull: { acceptedAthlete: { $in: deletedAthlete._id } } }, 
                { new: true }
            ).exec()
        ]);

        promises
            .then(promisesArr => {
                const appliedAthlete = promisesArr[0];
                const acceptedAthlete = promisesArr[1];

                const returnObj = {
                    resultArr: [appliedAthlete, acceptedAthlete],
                    deletedAthlete: deletedAthlete
                }

                return returnObj;
            })
            .then(obj => {
                deleteFromUser(obj.deletedAthlete)
            })
            .catch(err => resp(res, false, "something went wrong", err));
    }

    function deleteFromUser(deletedAthlete) {
        User.findByIdAndDelete(deletedAthlete.userId, (err, deletedUser) => {
            if (err) resp(res, false, "something went wrong", err);
            else resp(res, true, "user is deleted", deletedUser);
        });
    }
}

exports.athleteRemoveApply = (req, res) => {
    // this will take care of route '/athlete/:id/remove'
    Athlete.findByIdAndUpdate(req.params.id, {$pull: {appliedClub: {$in: req.clubId}}}, {new: true}, (err, updatedAthlete) => {
        if (err) resp(res, false, "something went wrong", err);
        else if (updatedAthlete !== null) updateClub(updatedAthlete);
        else resp(res, false, "athlete is not found", null);
    });

    Club.findByIdAndUpdate(req.clubId, {$pull: {appliedAthlete: {$in: updatedAthlete._id}}}, {new: true}, (err, updatedClub) => {
        if (err) resp(res, false, "something went wrong", err);
        else if (updatedClub !== null) resp(res, true, "remove process has succeed", [updatedAthlete, updatedClub]);
        else resp(res, false, "something went wrong", null);
    })
}

exports.athleteAccept = (req, res) => {
    /*
        This code will handle route '/athlete/:id/accept?apply=club' and 
        '/athlete/:id/accept?apply=scholarship&&id=${scholarshipId}'
        since an athlete candidate can apply directly to club or via scholarship.
    */
    Athlete.findByIdAndUpdate(req.params.id, {$push: {acceptedClub: req.clubId}}, {new: true}, (err, data) => {
        if (err) resp(res, false, "something went wrong", err);
        else if (data === null) resp(res, false, "athlete data is not found", null);
        else {
            if (req.query.apply === 'club') updateClub(data);
            else if (req.query.apply === 'scholarship') updateScholarship(data);
        }
    });

    function updateScholarship(data) {
        Scholarship.findByIdAndUpdate(req.query.id, {$push: {acceptedAthlete: data._id}}, {new: true}, (err, result) => {
            if (err) resp(res, false, "something went wrong", err);
            else if (result === null) resp(res, false, "scholarship data is not found", null);
            else updatedClub(data)
        });
    }

    function updateClub(data) {
        Club.findByIdAndUpdate(req.clubId, {$push: {acceptedAthlete: data._id}}, {new: true}, (err, result) => {
            if (err) resp(res, false, "something went wrong", err);
            else if (result === null) resp(res, false, "club data is not found", null);
            else resp(res, true, "application has been accepted", result);
        });
    }
}

exports.login = (req, res) => {
    const data = {
        token: req.data.token,
        athleteId: req.athleteId,
        userId: req.userId
    }

    resp(res, true, "you are logging in", data);
}