const mongoose = require('mongoose');

const ObjectId = mongoose.Schema.Types.ObjectId;
const clubSchema = new mongoose.Schema({
    //clubName: String,
    userId: {
        type: ObjectId,
        ref: 'User'
    },
    // sport id is needed to refer to its document in Sport model
    sportId: {
        type: ObjectId,
        ref: 'Sport'
    },
    clubLogo: {
        type: String,
        default: 'https://res.cloudinary.com/tim2canggih/image/upload/v1572016797/oyo3nau9uiwjmhmeghwb.png'
    },
    clubAddress: {
        type: String,
        trim: true
    },
    clubPhone: {
        type: String,
        trim: true
    },
    // a club can have different email for sign up purpose and as email contact
    clubEmail : {
        type: String
    },
    clubAbout: {
        type: String,
    },
    scholarshipOffered: [{
        type: ObjectId,
        ref: 'Scholarship'
    }],
    appliedAthlete: [{
        type: ObjectId,
        ref: 'Athlete'
    }],
    acceptedAthlete: [{
        type: ObjectId,
        ref: 'Athlete'
    }]
});

const Club = mongoose.model('Club', clubSchema);

module.exports = Club;