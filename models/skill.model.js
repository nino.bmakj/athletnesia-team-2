var mongoose = require('mongoose')

var skillSchema = new mongoose.Schema({
    skill: {
        type:String, 
        required:[true, "skill is required!"],
        minlength: [4, "skill minimum length is 4 characters"]
    }
   
  });

  var Skill = mongoose.model('Skill', skillSchema);

  module.exports = Skill