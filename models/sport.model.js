var mongoose = require('mongoose')

var sportSchema = new mongoose.Schema({
    sportName: {
        type:String, 
        required:[true, "sport is required!"],
        minlength: [4, "sport minimum length is 4 characters"],
        maxlength: [40, "sport maximum length is 20 characters"]
    }
  });

  var Sport = mongoose.model('Sport', sportSchema);

  module.exports = Sport