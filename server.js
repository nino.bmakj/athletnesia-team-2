const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors')
require('dotenv').config();

const PORT = process.env.PORT || 8000;

mongoose.set('useFindAndModify', false);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useNewUrlParser', true);

if (app.get('env') === 'development') {
  mongoose.connect(`mongodb://localhost/${process.env.LOCAL_DB_NAME}`);
} else {
  mongoose.connect(`mongodb+srv://${process.env.ATLAS_DB_USERNAME}:${process.env.ATLAS_DB_PASSWORD}@cluster0-bivyf.mongodb.net/${process.env.ATLAS_DB_NAME}`);
}

app.use(bodyParser.json());
app.use(cors())

app.get('/', (req, res) => {
  res.send('the app works~')
})

require('./routes/athlete.route')(app)
require('./routes/user.route')(app)
require('./routes/club.route')(app)
require('./routes/scholarship.route')(app)
require('./routes/sport.route')(app)
require('./routes/skill.route')(app)
require('./routes/search.route')(app)

app.listen(PORT, () => {
  console.log(`server works at port ${PORT}`);
});