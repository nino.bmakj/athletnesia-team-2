const club = require('../controllers/club.controller');
const userLogin = require('../middlewares/userLogin');
const authentication = require('../middlewares/authentication');
const authorization = require('../middlewares/authorization');
const clubAuth = require('../middlewares/clubAuth');
const athleteAuth = require('../middlewares/athleteAuth');
const applyCheck = require('../middlewares/applyCheck');

module.exports = app => {
  //app.post('/add-club', club.clubCreate);
  app.get('/clubs', club.clubShowAll);
  app.get('/club/:id', club.clubShow);
  app.put('/club/:id', authentication, authorization.club, club.clubEdit);
  app.delete('/club/:id', authentication, authorization.club, club.clubDelete);
  app.post('/club/login', userLogin, clubAuth, club.login);
  app.put('/club/:id/apply', authentication, athleteAuth, applyCheck.club, club.clubApply);
  app.put('/club/:id/remove', authentication, authorization.removeClub, club.clubRemoveApply);
}