const scholarship = require('../controllers/scholarship.controller');
const authentication = require('../middlewares/authentication');
const authorization = require('../middlewares/authorization');
const clubAuth = require('../middlewares/clubAuth');
const athleteAuth = require('../middlewares/athleteAuth');
const scholarshipAuth = require('../middlewares/scholarshipAuth');
const applyCheck = require('../middlewares/applyCheck');

module.exports = app => {
  app.get('/scholarships', scholarship.scholarshipShowAll);
  app.post('/add-scholarship', authentication, clubAuth, scholarship.scholarshipCreate);
  app.get('/scholarship/:id', scholarship.scholarshipDetail);
  app.put('/scholarship/:id', authentication, clubAuth, scholarshipAuth, authorization.scholarship, scholarship.scholarshipUpdate);
  app.delete('/scholarship/:id',authentication, clubAuth, scholarshipAuth, authorization.scholarship, scholarship.scholarshipDelete);
  app.put('/scholarship/:id/apply', authentication, athleteAuth, applyCheck.scholarship, scholarship.scholarshipApply);
  app.put('/scholarship/:id/remove', authentication, authorization.scholarship, scholarship.scholarshipRemoveApply);
}