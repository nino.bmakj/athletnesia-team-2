const user = require('../controllers/user.controller');
const authentication = require('../middlewares/authentication')
const authorization = require('../middlewares/authorization');
const emailCheck = require('../middlewares/emailCheck');

module.exports = app => {
  app.get('/users', user.userShowAll);
  app.get('/user/:id', user.userShow);
  app.put('/user/:id', authentication, authorization.user, emailCheck, user.userEdit);
  app.post('/sign-up', user.userCreate);
}