const skill = require('../controllers/skill.controller');

module.exports = app => {
  app.post('/add-skill', skill.skillCreate)
  app.get('/skills', skill.skillShowAll)
  app.get('/skill/:id', skill.skillShow)
  app.delete('/skill/:id', skill.skillDelete)
  app.put('/skill/:id', skill.skillEdit)
}