const sport = require('../controllers/sport.controller')

module.exports = app => {
    app.post('/add-sport', sport.sportCreate)
    app.get('/sports', sport.sportShowAll)
    app.get('/sport/:id', sport.sportShow)
    app.delete('/sport/:id', sport.sportDelete)
    app.put('/sport/:id', sport.sportEdit)
}