const Athlete = require('../models/athlete.model');
const Club = require('../models/club.model');
const Scholarship = require('../models/scholarship.model');
const responseHandling = require('../middlewares/responseHandling');

module.exports = app => {
  // this router will handle '/search?q=${query}&&filter=${filter}'
  app.get('/search', (req, res) => {
    const q = req.query.q;

    if (req.query.filter === 'athlete') {
      Athlete.find(
        {$text: 
          {$search: 
            {$regex: new RegExp(q)}
          }
        })
        .then(result => {
          if (result.length === 0) responseHandling(res, false, "there is no match result", []);
          else responseHandling(res, true, "query result is found", result);
        })
        .catch(err => responseHandling(res, false, "something went wrong", err));
    }
    else if (req.query.filter === 'club') {
      Club.find(
        {$text: 
          {$search: 
            {$regex: new RegExp(q)}
          }
        })
        .then(result => {
          if (result.length === 0) responseHandling(res, false, "there is no match result", []);
          else responseHandling(res, true, "query result is found", result);
        })
        .catch(err => responseHandling(res, false, "something went wrong", err));
    }
    else {
      Scholarship.find(
        {$text: 
          {$search: 
            {$regex: new RegExp(q)}
          }
        })
        .then(result => {
          if (result.length === 0) responseHandling(res, false, "there is no match result", []);
          else responseHandling(res, true, "query result is found", result);
        })
        .catch(err => responseHandling(res, false, "something went wrong", err));
    }
  });
}