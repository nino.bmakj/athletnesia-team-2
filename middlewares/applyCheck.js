const responseHandling = require('./responseHandling');
const Scholarship = require('../models/scholarship.model');
const Club = require('../models/club.model');

const scholarship = (req, res, next) => {
	Scholarship.findById(req.params.id, (err, data) => {
		if (err) responseHandling(res, false, "something went wrong", err);
		else if (data === null) responseHandling(res, false, "scholarship data is not found", null);
		else {
			const hasApplied = data.appliedAthlete.find(athlete => athlete === req.athleteId);
			if (hasApplied) responseHandling(res, false, "you have applied this scholarship", null);
			else next();
		}
	});
}

const club = (req, res, next) => {
	Club.findById(req.params.id, (err, data) => {
		if (err) responseHandling(res, false, "something went wrong", err);
		else if (data === null) responseHandling(res, false, "club data is not found", null);
		else {
			const hasApplied = data.appliedAthlete.find(athlete => athlete === req.athleteId);
			if (hasApplied) responseHandling(res, false, "you have applied this club", null);
			else next();
		}
	});
}

module.exports = { scholarship, club }