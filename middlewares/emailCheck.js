const User = require('../models/user.model');
const responseHandling = require('./responseHandling');

const emailCheck = (req, res, next) => {
  User.findOne({email: req.body.email}, (err, data) => {
    if (err) responseHandling(res, false, "something went wrong", err);
    else if (data !== null) {
      if (data._id !== req.params.id) responseHandling(res, false, "email has already been taken", null);
      else responseHandling(res, true, "email is safe to use", data);
    } 
    else responseHandling(res, true, "email is safe to use", data); 
  })
}

module.exports = emailCheck;