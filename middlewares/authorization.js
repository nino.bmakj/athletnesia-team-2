const User = require('../models/user.model');
const Athlete = require('../models/athlete.model');
const Club = require('../models/club.model');
const Scholarship = require('../models/scholarship.model');
const Admin = require('../models/admin.model');
//const authorizationTemp = require('./authorizationTemp');

const authorizationTemp = (res, model, paramsId, userId, next) => {
  model.findById(paramsId)
    .then(data => {
      if (!data) responseHandling(res, false, "there is no associated data avalaible");
      else {
        if (String(data.userId) === String(userId) || String(data._id) === String(userId)) next();
        else responseHandling(res, false, "you are not authorized");
      }
    })
    .catch(err => responseHandling(res, false, "something went wrong, no data is retrieved", err));
}

const user = (req, res, next) => {
  authorizationTemp(res, User, req.params.id, req.userId, next);
}

const athlete = (req, res, next) => {
  authorizationTemp(res, Athlete, req.params.id, req.userId, next);
}

const club = (req, res, next) => {
  authorizationTemp(res, Club, req.params.id, req.userId, next);
}

const scholarship = (req, res, next) => {
  Scholarship.findById(req.params.id)
    .then(data => {
      if (!data) responseHandling(res, false, "scholarship data is not found", null);
      else {
        if (String(data.clubId) === String(req.clubId)) next();
        else responseHandling(res, false, "you are not authorized", null);
      }
    })
    .catch(err => responseHandling(res, false, "something went wrong", err));
}

const admin = (req, res, next) => {
  authorizationTemp(res, Admin, req.adminId, req.userId, next);
}

const removeAthlete = (req, res, next) => {
  Athlete.findById(req.params.id)
    .then(data => {
      if (!data) responseHandling(res, false, "athlete data is not found", null);
      else {
        const isApplied = data.appliedClub.find(club => String(club) === String(req.clubId));
        if (isApplied) next();
        else responseHandling(res, false, "you are not authorized", null);
      }
    })
    .catch(err => responseHandling(res, false, "something went wrong", err));
}

const removeClub = (req, res, next) => {
  Club.findById(req.params.id)
    .then(data => {
      if (!data) responseHandling(res, false, "club data is not found", null);
      else {
        const isApplied = data.appliedAthlete.find(athlete => String(athlete) === String(req.athleteId));
        if (isApplied) next();
        else responseHandling(res, false, "you are not authorized", null);
      }
    })
    .catch(err => responseHandling(res, false, "something went wrong", err));
}

module.exports = { user, athlete, club, scholarship, removeAthlete, removeClub }